# med-tracker

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### How to use
A simple attention/meditation tracker.
After setting your meditation duration and clicking play, you can begin to meditate. Tap anywhere on the screen each time you lose your focus or your mind wanders, then bring your attention back to your breath.



## To Do

```
Pause meditation
Store previous meditation sessions
infinite time set (for the monks trancending to 'tukdam')


```
